<h1>myPercentile</h1>

<p>
myPercentile is a simple percentile counter, which runs a
fluid animation from 0 to the percentile of your choice.
Features include the ability to change and adjust the color,
size, and font utilized throughout this little program, and
it can be easily incorporated throughout numerous canvas contexts,
while also being easily installable throughout either your web-page
or web-application.
</p>

<h1>Live Demo</h1>

<p>
<a href="http://www.byrne-systems.com/myPercentile/">http://www.byrne-systems.com/myPercentile/</a>
</p>