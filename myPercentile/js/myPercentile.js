/**
 * @package                 {Object Literal}            myPercentile JS Library
 * @author                  'Justin Byrne'              justinbyrne001@gmail.com
 * @link                    'Github'                    https://github.com/justinbyrne001/myPercentile
 * @license                 'MIT License'               https://github.com/justinbyrne001/myPercentile/blob/master/LICENSE.md
 * @version                                             1
 */

/* Canvas: percentages counter */
window.addEventListener('load', eventWindowLoaded, false);

function eventWindowLoaded() { percentageCounter(); }

function canvasSupport() { return Modernizr.canvas; }

function percentageCounter() {

    // if: no canvas support return, else declare 2D canvas
    if (!canvasSupport()) { return; } else {

        // Declare: canvases array for each DOM canvas element
        var canvases=[];
            canvases.push(document.getElementById('cvs-0'));
            canvases.push(document.getElementById('cvs-1'));
            canvases.push(document.getElementById('cvs-2'));
            canvases.push(document.getElementById('cvs-3'));
            canvases.push(document.getElementById('cvs-4'));
            canvases.push(document.getElementById('cvs-5'));
            canvases.push(document.getElementById('cvs-6'));

        // Declare: contexts array for each 2D API context
        var contexts=[];

        // Set: each contexts with a 2D canvas API
        for (var i=0; i<canvases.length; i++) {
            contexts.push(canvases[i].getContext('2d'));
        }
    }

    // Declare: stepStart & stepEnd arrays for animations
    var stepStart=[];
    var stepEnd=[];

    /**
     * myPercentage() description]
     * @param                   {Number}                    Percentage: to be incrementally increased throughout the animation
     * @param                   {Object} ctx                Context: the appropriate context to display the percentage counter
     */
    var myPercentage = function(percentage,ctx) {
        this.context = ctx;
        this.lineWidth = 10;
        this.strokeStyle = '#2B5981';
        this.percentage = percentage;
        this.degrees = '';
        this.radians = '';
        this.x = 57.5;
        this.y = 57.5;
        this.r = 45;
        this.s = 1.5 * Math.PI;
        this.drawShape = function() {
            function clearCanvas(x,y,width,height) {
                contexts[ctx].fillStyle = '#FCFCFC';
                contexts[ctx].fillRect(x,y,width,height);
            }
            function circle(x,y,r,startAngle,endAngle,counterClockwise) {
                contexts[ctx].beginPath();
                contexts[ctx].arc(x,y,r,startAngle,endAngle,counterClockwise);
                contexts[ctx].shadowColor = '#242424';
                contexts[ctx].shadowBlur = 7;
                contexts[ctx].stroke();
                contexts[ctx].closePath();
            }
            function writePercentage(percentage) {
                contexts[ctx].font = '20px Arial';
                contexts[ctx].fillStyle = 'black';
                if (percentage > 0.99) {
                    contexts[ctx].fillText(Math.round(percentage*100)+'%',33,63);
                } else {
                    contexts[ctx].fillText(Math.round(percentage*100)+'%',40,63);
                }
            }
            contexts[ctx].strokeStyle = this.strokeStyle;
            contexts[ctx].lineWidth = this.lineWidth;
            clearCanvas(0,0,115,115);
            this.degrees = this.percentage * 360.0;
            this.radians = this.degrees * (Math.PI / 180);
                circle(this.x,this.y,this.r,this.s,this.radians+this.s,false);
                writePercentage(this.percentage);
                contexts[ctx].save();
        };
    };

    // Set: stepStart & stepEnd for animation(s)
    function setSteps(object) {
        stepStart[object.context] = 0;
        stepEnd[object.context] = object.percentage * 100;
        object.percentage = 0.00;
    }

    //Instantiate: new percentages
    var htmlPerc = new myPercentage(0.95,0);                // HTML5
    var cssPerc = new myPercentage(0.90,1);                 // CSS3
    var jsPerc = new myPercentage(0.85,2);                  // JavaScript
    var jqPerc = new myPercentage(0.75,3);                  // jQuery
    var phpPerc = new myPercentage(0.70,4);                 // PHP
    var sqlPerc = new myPercentage(0.60,5);                 // SQL
    var javaPerc = new myPercentage(0.50,6);                // Java

    // Initialize: animation with the encapsulated object
    function beginAnimation(object) {

        setSteps(object);                                   // Set: stepStart & stepEnd variables for animation(s)

        // Execute: animation process using the 'object' argument
        setInterval(function() {
            if (stepStart[object.context] < stepEnd[object.context]) {
                object.percentage += 0.01;
                object.drawShape();
                stepStart[object.context]++;
            }
        }, 33);
    }

    // Initialize: beginAnimation(object(s))
    beginAnimation(htmlPerc);
    beginAnimation(cssPerc);
    beginAnimation(jsPerc);
    beginAnimation(jqPerc);
    beginAnimation(phpPerc);
    beginAnimation(sqlPerc);
    beginAnimation(javaPerc);
}